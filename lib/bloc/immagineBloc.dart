import 'package:image_uploader/model/DataModel.dart';
import 'package:rxdart/rxdart.dart';
import 'package:image_uploader/persistence/Repository.dart';

class ImmagineBloc {
  Repository _repository = Repository();

  final _nomiImmagini = BehaviorSubject<List<ImmagineName>>();

  BehaviorSubject <List<ImmagineName>> get immaginiName => _nomiImmagini.stream;

  getNomiImmagini() async {
    List<ImmagineName> nomiImmagini = await _repository.getImmaginiName();
    _nomiImmagini.sink.add(nomiImmagini);
  }
}
