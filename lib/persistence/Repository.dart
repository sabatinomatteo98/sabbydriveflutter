import 'package:image_uploader/model/DataModel.dart';
import 'NetworkRequest.dart';

class Repository {
  NetworkRequest networkRequest = NetworkRequest();

  Future<List<ImmagineName>> getImmaginiName() => networkRequest.getImmaginiName();
}