import 'dart:convert';

import 'package:image_uploader/model/DataModel.dart';
import 'package:http/http.dart' as http;
import 'package:image_uploader/persistence/NetworkAddress.dart';
class NetworkRequest {

  Future <List<ImmagineName>> getImmaginiName() async {
    print(NetworkAddress.getNomeImmagini);
    final response = await http.get(NetworkAddress.getNomeImmagini);
    if (response.statusCode == 200) {
      print(response.body);
      return (json.decode(response.body) as List).map((i) =>
          ImmagineName.fromJson(i)).toList();
    } else {
      throw Exception('Failed to load the name of immages');
    }
  }
}