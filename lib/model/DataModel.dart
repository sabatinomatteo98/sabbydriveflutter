
class ImmagineName {

  final String fileName;
  ImmagineName({this.fileName});

  factory ImmagineName.fromJson(Map<String, dynamic> json){
    return ImmagineName(
        fileName: json['nomeFile'].toString()
    );
  }
}