  import 'dart:convert';
  import 'dart:typed_data';
  import 'package:http/http.dart' as http;
  import 'package:flutter/material.dart';
  import 'package:http/http.dart';
  import 'package:http_parser/http_parser.dart';
  import 'package:image_uploader/bloc/immagineBloc.dart';
  import 'package:image_uploader/model/DataModel.dart';
import 'package:image_uploader/ui/selectPhoto.dart';
  import 'dart:async';

  import 'package:multi_image_picker/multi_image_picker.dart';

  void main() => runApp(new MyApp());

  class MyApp extends StatefulWidget {
    @override
    _MyAppState createState() => new _MyAppState();
  }

  class _MyAppState extends State<MyApp> {
    List<Tab> _tabs = List<Tab>();
    var _index = 0;
    @override
    void initState() {

      super.initState();

    }


    void setIndex(int index){
      setState(() {
        _index = index;
      });
    }

    List<Tab> getTabs(int count) {
      _tabs.clear();
      for (int i = 0; i < count; i++) {
        _tabs.add(getTab(i));
      }
      return _tabs;
    }

    Tab getTab(int index){
      var text = index == 1 ? "server" : "locale";
      return Tab(text: text);
    }

    @override
    Widget build(BuildContext context) {
      _tabs = getTabs(2);
      return new MaterialApp(
        home: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              title: Text('Sabby Drive'),
              backgroundColor: Colors.blueAccent,

              bottom: TabBar(
                  isScrollable: false,
                  indicatorColor: Colors.white,
                  onTap: (int index){
                    _index = index;
                    setIndex(index);
                    //TODO chiamata http per colli del cedi
                  },
                  tabs: _tabs)
            ),
            body: selectPhoto(),
          ),

          )
        );
    }
  }
