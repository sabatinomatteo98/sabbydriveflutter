import 'dart:convert';
import 'dart:typed_data';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_uploader/bloc/immagineBloc.dart';
import 'package:image_uploader/model/DataModel.dart';
import 'dart:async';

import 'package:multi_image_picker/multi_image_picker.dart';

void main() => runApp(new selectPhoto());

class selectPhoto extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<selectPhoto> {
  List<Asset> images = List<Asset>();
  Uri url = new Uri.http("192.168.178.117:8888", "/sabbydrive/immagini");
  ImmagineBloc _immagineBloc = ImmagineBloc();
  int _cont = 0;
  @override
  void initState() {
    _immagineBloc.getNomiImmagini();
    super.initState();

  }

  List<Asset> getImmaginiDaCaricare(List<Asset> immaginiSelezionate, AsyncSnapshot<dynamic> snapshot){
    List<Asset> immaginiDaCaricare = new List<Asset>();
    for (int i = 0; i<immaginiSelezionate.length; i++){
      bool caricare = isPresent(immaginiSelezionate[i], snapshot);
      if (caricare == false){
        immaginiDaCaricare.add(immaginiSelezionate[i]);
      }
    }
    return immaginiDaCaricare;
  }

  bool isPresent(Asset immagineDaCaricare, AsyncSnapshot<dynamic> snapshot){
    var immaginiUploaded = snapshot.data;
    bool trovato = false;
    var i=0;
    while (!trovato && i < immaginiUploaded.length){
      if(immaginiUploaded[i].fileName == immagineDaCaricare.name){
        trovato = true;
      }else{
        i++;
      }
    }
    return trovato;
  }

  Widget buildGridView() {
    return GridView.count(
      crossAxisCount: 3,
      children: List.generate(images.length, (index) {
        Asset asset;
        asset = images[index];
        return AssetThumb(
          asset: asset,
          width: 300,
          height: 300,
        );
      }),
    );
  }




  void removeAssets() {
    setState(() {
      images.clear();
    });
  }

  Future<void> upload() async {
    if (null != images) {
      for (Asset asset in images) {
        ByteData byteData = await asset.getByteData();
        List<int> imageData = byteData.buffer.asUint8List();
        var request = http.MultipartRequest('POST', url)
          ..fields['fileName'] = asset.name
          ..files.add(await http.MultipartFile.fromBytes('image', imageData));
        var response = await request.send();
        if (response.statusCode == 200){
          print("Photo uploaded");
          removeAssets();
          setState(() {
            _cont++;
          });
        }else {
          print('Failed to upload Photo.');
        }
      }
    }
  }


  Future<void> loadAssets(AsyncSnapshot<dynamic> snapshot) async {
    List<Asset> resultList = List<Asset>();
    String error = '';
    print(snapshot.data[0].fileName);
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Select Photo",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }
    if (!mounted) return;

    setState(() {
      images = getImmaginiDaCaricare(resultList, snapshot);
    });
  }

  @override
  Widget build(BuildContext context) {
    if(_cont > 0){
      _immagineBloc.getNomiImmagini();
    }
    return StreamBuilder(
        stream: _immagineBloc.immaginiName,
        builder: (context, snapshot){
      if (snapshot.hasData) {
        return Container(
          child: images.length > 0 ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                child: Text("Seleziona le foto"),
                onPressed: () => loadAssets(snapshot),
              ),
              RaisedButton(
                child: Text("Rimuovi le foto"),
                onPressed: removeAssets,
              ),
              RaisedButton(
                child: Text("Carica le foto"),
                onPressed: upload,
              ),
              Expanded(
                child: buildGridView(),
              )
            ],
          ) : Center(
              child: RaisedButton(
                child: Text("Seleziona le foto"),
                onPressed: () => loadAssets(snapshot),
              )
          ),
        );
      }
      if(snapshot.hasError){
        return Text(snapshot.error.toString());
      }
      return Center(
          child: Text(snapshot.error.toString())
      );
    });
  }
}
